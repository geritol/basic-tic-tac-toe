"use strict";

const playState = {
  player: "x",
  board: [
    [0, 0, 0],
    [0, 0, 0],
    [0, 0, 0],
  ],
};

const iterateLines = (state, callback) => {
  state.board.forEach((line, xIndex) => callback(line, xIndex));
};

const iterateNodes = (state, callback) => {
  iterateLines(state, (line, xIndex) => {
    line.forEach((node, yIndex) => {
      callback(node, xIndex, yIndex);
    });
  });
};

const iterate2DDirections = (callback) => {
  const directions = [-1, 0, 1];

  directions.forEach((xDirection) => {
    directions.forEach((yDirection) => {
      if (xDirection === 0 && yDirection === 0) return;
      callback(xDirection, yDirection);
    });
  });
};

const renderState = (state) => {
  const playGround = document.querySelector(".playground");
  clear(playGround);
  iterateLines(state, (line, xIndex) => {
    const lineElement = document.createElement("div");
    lineElement.classList.add("line");
    playGround.appendChild(lineElement);
    line.forEach((node, yIndex) => {
      const nodeElement = document.createElement("div");
      nodeElement.classList.add("node");
      nodeElement.appendChild(document.createTextNode(node));
      nodeElement.addEventListener("click", () =>
        handleMove(state, xIndex, yIndex)
      );
      lineElement.appendChild(nodeElement);
    });
  });
};

const clear = (element) => {
  element.innerHTML = "";
};

const getWinner = (state) => {
  const requiredToWin = 3;
  let winner = null;
  iterateNodes(state, (node, xIndex, yIndex) => {
    if (node === 0) return;
    iterate2DDirections((xDirection, yDirection) => {
      for (let i = 0; i < requiredToWin; i++) {
        const currentLine = state.board[xIndex + xDirection * i];
        if (!currentLine) break;
        const currentNode = currentLine[yIndex + yDirection * i];
        if (currentNode !== node) break;
        if (i === requiredToWin - 1) {
          winner = node;
          break;
        }
      }
    });
  });
  return winner;
};

const handleMove = (state, x, y) => {
  if (state.board[x][y] !== 0) return;
  state.board[x][y] = state.player;
  state.player = state.player === "x" ? "o" : "x";
  renderState(playState);
  const winner = getWinner(playState);
  if (winner) alert(`player ${winner} won`);
};

renderState(playState);
